
import { reverse } from 'kitten-critic2/helpers/reverse';
import { module, test } from 'qunit';

module('Unit | Helper | reverse');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = reverse([42]);
  assert.ok(result);
});

