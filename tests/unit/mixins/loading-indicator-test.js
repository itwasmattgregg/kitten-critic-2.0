import Ember from 'ember';
import LoadingIndicatorMixin from 'kitten-critic2/mixins/loading-indicator';
import { module, test } from 'qunit';

module('Unit | Mixin | loading indicator');

// Replace this with your real tests.
test('it works', function(assert) {
  let LoadingIndicatorObject = Ember.Object.extend(LoadingIndicatorMixin);
  let subject = LoadingIndicatorObject.create();
  assert.ok(subject);
});
