import Ember from 'ember';

export default Ember.Controller.extend({
    sortProps: ['vote:desc'],
    sortedCats: Ember.computed.sort('model', 'sortProps')
  });
