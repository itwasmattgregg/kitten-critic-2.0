import DS from 'ember-data';

let {
  Model,
  attr
} = DS;

export default Model.extend({
  url: attr('string'),
  vote: attr('number'),
  createdDate: attr('date'),
  voteIDs: DS.hasMany('vote')
});
