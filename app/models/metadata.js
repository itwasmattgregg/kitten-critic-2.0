import DS from 'ember-data';

export default DS.Model.extend({
  totalCats: DS.attr('number'),
});
