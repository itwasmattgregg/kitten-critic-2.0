import DS from 'ember-data';

export default DS.Model.extend({
  userID: DS.attr(),
  score: DS.attr(),
  cat: DS.belongsTo('cat')
});
