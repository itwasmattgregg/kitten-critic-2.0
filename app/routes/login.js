import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    signIn: function(provider) {
      this.get('session').open('firebase', { provider: provider});
    },
  }
});
