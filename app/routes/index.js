import Ember from 'ember';
import progress from 'ember-cli-nprogress';

export default Ember.Route.extend({
  // firebaseApp: Ember.inject.service(),

  beforeModel: function() {
    return this.get('session')
      .fetch()
      .catch(function() {});
  },
  activate: function() {
    // const storageRef = this.get('firebaseApp').storage();
    if (!this.get('session').get('isAuthenticated')) {
      Ember.$('body').toggleClass('splash_page');
      Ember.$('html').toggleClass('splash_page');
    }
  },
  deactivate: function() {
    Ember.$('body').removeClass('splash_page');
    Ember.$('html').removeClass('splash_page');
  },

  model() {
    progress.start();
    return {
      cats: this.store.findAll('cat').then(cats => {
        let catArray = [];
        for (let i = 4; i >= 0; i--) {
          const rand = Math.floor(Math.random() * cats.get('length'));
          catArray[i] = cats.objectAt(rand);
        }
        progress.done();
        return catArray.toArray();
      })
    };
  },

  actions: {
    signIn: function(provider) {
      this.get('session').open('firebase', { provider: provider });
    },
    signOut: function() {
      this.get('session').close();
    },

    // get another random cat from loaded array of all cats
    // and add to array of 5 cats in DOM then reset model which will
    // refresh DOM
    anotherPlease: function() {
      const controller = this.controllerFor('index');
      let allCats = this.store.peekAll('cat');
      let cats = controller.get('model.cats');

      cats.then(response => {
        let catArray = response.toArray();
        const rand = Math.floor(Math.random() * allCats.get('length'));
        catArray.pop();
        catArray.unshift(allCats.objectAt(rand));
        let promise = new Ember.RSVP.Promise(function(resolve) {
          resolve(catArray);
        });
        controller.set('model.cats', promise);
      });
    },
    voteYes: function(cat) {
      let that = this;
      this.store.findRecord('cat', cat.id).then(function(currentCat) {
        currentCat.incrementProperty('vote');
        currentCat.save();

        let vote = that.store.createRecord('vote', {
          userID: that.get('session').get('currentUser').uid,
          score: 1,
          cat: currentCat
        });
        vote.save();
      });
    },
    voteNo: function(cat) {
      let that = this;
      this.store.findRecord('cat', cat.id).then(function(currentCat) {
        currentCat.decrementProperty('vote');
        currentCat.save();

        let vote = that.store.createRecord('vote', {
          userID: that.get('session').get('currentUser').uid,
          score: -1,
          cat: currentCat
        });
        vote.save();
      });
    }
  }
});
