import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function() {
    let that = this;
    let session = this.get('session').fetch().then(function(){
      if(that.get('session').get('currentUser').uid != 'aDakqudNqJPcNvuFoXD8kgCNP4g1') {
        that.transitionTo('index');
      }
    });
    return session;
  },
  model() {
    return {
      cat: {}
    }
  },
  actions: {
    createCat(cat) {
      let newCat = this.store.createRecord('cat', {
        url: cat.url,
        createdDate: new Date()
      });
      newCat.save();
      this.store.findRecord('metadata', 'fdaf34fds').then(function(totalCats) {
        totalCats.incrementProperty('totalCats');
        totalCats.save();
      });
    },
  }
});
