import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function() {
    let that = this;
    let session = this.get('session').fetch().then(function(){
      if(that.get('session').get('currentUser').uid != 'aDakqudNqJPcNvuFoXD8kgCNP4g1') {
        that.transitionTo('index');
      }
    });
    return session;
  },

	model() {
    // return this.store.query('cat', {limitToFirst: 10});
    return this.store.findAll('cat');
  },

  actions: {
    getACatsCatAPI() {
      let scope = this;
      let url = '//thecatapi.com/api/images/get?api_key=ODk3OTU&format=xml&results_per_page=1';

      function tryForACat () {
        let cat = new Ember.RSVP.Promise(function(resolve, reject) {
          var xhr = new XMLHttpRequest();

          xhr.open('GET', url);
          xhr.onreadystatechange = handler;
          xhr.responseType = 'xml';
          xhr.send();

          function handler() {
            if (this.readyState === this.DONE) {
              if (this.status === 200) {
                resolve({
                  url: Ember.$(this.response).find('url').text()
                });
              } else {
                reject(new Error('getXML: `' + url + '` failed with status: [' + this.status + ']'));
              }
            }
          }
        });
        return cat.then(function(url) {
          if(imageExists(url.url)) {

            let newCat = scope.store.createRecord('cat', {
              url: url.url,
              createdDate: new Date()
            });
            newCat.save();

            scope.store.findRecord('metadata', 'fdaf34fds').then(function(totalCats) {
              totalCats.incrementProperty('totalCats');
              totalCats.save();
            });

          } else {
            tryForACat();
          }
        }, function (error) {
          return error;
        });
      }


      let count = 0;

      let multipleCatsFunction = function() {
        if (count < 100) {
          tryForACat();
          count++;
          multipleCatsFunction();
        }
      }

      return multipleCatsFunction();



      function imageExists(image_url) {

        var http = new XMLHttpRequest();
        try {
          http.open('HEAD', image_url, false);
          http.send();
        } catch (e){
          return false;
        }

        return http.status != 404;

      }
    },
    // loadMore() {
    //   let controller = this.controllerFor('get-cats');
    //   let currentLimit = controller.get('model').query.limitToFirst;
    //   this.store.query('cat', {limitToFirst: currentLimit + 25}).then((cats) => {
    //     controller.set('model', cats);
    //   });
    // },
    decrementCatsPlease() {
      this.get('store').findRecord('metadata', 'fdaf34fds').then(function(totalCats) {
        totalCats.decrementProperty('totalCats');
        totalCats.save();
      });
    },

    duplicates() {
      let that = this;

      this.get('store').findAll('cat',{ reload: true }).then(function(list){
          const array = list.toArray();
          const sorted = array.sort((a, b) => a.get('url') > b.get('url') ? 1 : -1);
          let lastItem = sorted[0];

          for(let i = 1; i < sorted.length; i++){
            if(sorted[i].get('url') === lastItem.get('url')) {
              console.log(sorted[i].id + ":" + sorted[i].get('url') + "\n" + lastItem.id + ":" + lastItem.get('url'));
              that.store.findRecord('cat', lastItem.id).then(function(doomedCat){
                doomedCat.destroyRecord();
                that.send('decrementCatsPlease');
              });
            }
            lastItem = sorted[i];
          }
      });
    }
  }
});
