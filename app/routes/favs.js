import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function() {
    return this.get('session').fetch().catch(function() {});
  },
  model() {
    return this.store.query('cat', {orderBy: 'vote', startAt: 1, limitToLast: 10});
  },
});
