import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('get-cats');
  this.route('add-cat');
  this.route('login');
  this.route('favs');
  this.route("errors/fourOhFour", { path: "*path"});
});

export default Router;
