import Ember from 'ember';

export default Ember.Component.extend({
  didInsertElement: function() {
    window.addEventListener('keyup', vote);
    let that = this;
    function vote(event) {
      if (event.keyCode == '37') {
        that.sendAction('voteNo', that.get('cats').objectAt(4));
        that.sendAction('anotherPlease');
      }
      if (event.keyCode == '39') {
        that.sendAction('voteYes', that.get('cats').objectAt(4));
        that.sendAction('anotherPlease');
      }
    }
  },
  actions: {
    swipeLeft(cats) {
      this.sendAction('voteNo', cats[4]);
      this.sendAction('anotherPlease');
    },
    swipeRight(cats) {
      this.sendAction('voteYes', cats[4]);
      this.sendAction('anotherPlease');
    }
  }
});
