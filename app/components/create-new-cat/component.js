import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    createCat(cat) {
      this.sendAction('createCat', cat);
      this.set('cat', {});
    }
  }
});
