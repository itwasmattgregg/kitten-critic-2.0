import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		removeCat(cat) {
      cat.deleteRecord();
      cat.save();
      this.sendAction('decrementCatsPlease');
		}

}});
