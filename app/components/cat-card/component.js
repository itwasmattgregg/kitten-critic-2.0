import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['kitten-card'],
  classNameBindings: ['lastCard'],
  lastCard: Ember.computed(function() {
    const index = this.get('index');
    if (index == 4) return true;
  })
});
